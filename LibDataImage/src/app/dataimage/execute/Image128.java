package app.dataimage.execute;

/**
 *
 * @author Tarcisio
 */
public class Image128 {
    
    public static final String ICONE_APLICATIVO = "/app/dataimage/execute/x128/appIcon.png";
    public static final String ICONE_SOBRE = "/app/dataimage/execute/x128/infoIcon.png";
    public static final String ICONE_SEARCH = "/app/dataimage/execute/x128/searchIcon.png";
    
}
