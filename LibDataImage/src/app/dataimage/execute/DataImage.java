package app.dataimage.execute;

import java.awt.*;
import java.net.URL;
import javax.swing.*;

/**
 *
 * @author Tarcisio
 */
public class DataImage {
    
    public Image iconeJanela(String urlIcone){
        try{
        
            URL imagemIconeJanela = Class.class.getResource(urlIcone);
            Image imagemTituloJanela = Toolkit.getDefaultToolkit().getImage(imagemIconeJanela);
            return imagemTituloJanela;
            
        }catch(Exception erro){
            
            JOptionPane.showMessageDialog(null, "Erro!", "Não foi possível carregar o ícone da janela, assinatura\n"
                    + "do erro: "+erro.getMessage(), JOptionPane.ERROR_MESSAGE);
            
            return null;
            
        }
    }
    
    public Icon iconeComponente(String urlIcone){
        
        try{
        
            URL imagemIconeJanela = Class.class.getResource(urlIcone);
            Icon imagemTituloJanela = new ImageIcon(imagemIconeJanela);
            return imagemTituloJanela;
            
        }catch(Exception erro){
            
            JOptionPane.showMessageDialog(null, "Erro!", "Não foi possível carregar o ícone da janela, assinatura\n"
                    + "do erro: "+erro.getMessage(), JOptionPane.ERROR_MESSAGE);
            
            return null;
            
        }
    }
    
}
