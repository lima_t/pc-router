package app.dataimage.execute;

/**
 *
 * @author Tarcisio
 */
public class Image24 {
    
    public static final String ITEM_BANDEJA = "/app/dataimage/execute/x24/trayIcon.png";    
    public static final String ITEM_SAIR = "/app/dataimage/execute/x24/quitIcon.png";
    public static final String ITEM_MANUAL = "/app/dataimage/execute/x24/helpIcon.png";
    public static final String ITEM_SOBRE = "/app/dataimage/execute/x24/infoIcon.png";
    public static final String ITEM_SUPORTE = "/app/dataimage/execute/x24/homePageIcon.png";
    public static final String ITEM_SEARCH = "/app/dataimage/execute/x24/searchIcon.png";
    public static final String ITEM_LIMPAR = "/app/dataimage/execute/x24/eraseIcon.png";
    
}
