package app.dataimage.execute;

/**
 *
 * @author Tarcisio
 */
public class Image36 {
    
    public static final String BT_TURN_ON = "/app/dataimage/execute/x36/turnOnIcon.png";
    public static final String BT_TURN_OFF = "/app/dataimage/execute/x36/turnOffIcon.png";
    
}
