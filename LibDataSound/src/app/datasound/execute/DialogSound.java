package app.datasound.execute;

/**
 *
 * @author Tarcísio de Lima
 */
public class DialogSound {
    
    public final static String DIALOG_ERROR = "/app/datasound/execute/dialogs/errorDialog.wav";
    public final static String DIALOG_INFORMATION = "/app/datasound/execute/dialogs/informationDialog.wav";
    public final static String DIALOG_SUCESS = "/app/datasound/execute/dialogs/sucessDialog.wav";
    public final static String DIALOG_WARNING = "/app/datasound/execute/dialogs/warningDialog.wav";
}
