package app.pcrouter.principal;

import app.datasound.execute.DataSound;
import app.datasound.execute.DialogSound;
import app.pcrouter.banco.ConexaoSQLite;
import app.pcrouter.banco.dao.CacheDao;
import app.pcrouter.telas.TelaPrincipal;
import java.io.File;
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;

/**
 *
 * @author Tarcisio
 */
public class Principal {
    
    private static final File ARQUIVO_BANCO = new File("dbCache.db");
    private static final ConexaoSQLite OBJ_CONEXAO_SQLITE = new ConexaoSQLite();

    public static void main(String[] args) {
        
        try {
            
            if(ARQUIVO_BANCO.exists() == false){
                OBJ_CONEXAO_SQLITE.conectarBancoDados();
                OBJ_CONEXAO_SQLITE.criarBancoDados(null);
                OBJ_CONEXAO_SQLITE.fecharConexao(null);
            }
            
            new CacheDao().carregarCache(null);
            
            UIManager.setLookAndFeel(new MetalLookAndFeel());
            new TelaPrincipal().setVisible(true);
            
        } catch (Exception erroLookAndFeel) {
            
            new DataSound().reproduzirSom(null, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(null, "Erro Fatal","Um erro inesperado ocorreu e não foi possível iniciar"
                    + " o programa\nAssinatura do erro: "+erroLookAndFeel.getMessage(), JOptionPane.ERROR_MESSAGE);
            
        }
        
    }
    
}
