package app.pcrouter.banco;

import app.datasound.execute.DataSound;
import app.datasound.execute.DialogSound;
import java.awt.Component;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 * Classe responsavel por armazenar metodos e funções referentes as rotinas do banco de dados.
 * 
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class ConexaoSQLite {
    //Objetos e Variaveis Globais
    public static Statement comandoSql;
    public static Connection conexaoBanco;
    
    private final String nomeBanco = "dbCahe.db";
    
    /**
     * Metodo responsavel por efetuar a conexão com o banco de dados SQLite 3, ele recebe como parametro um 
     * objeto do tipo Component que por sua vez é a tela referente na qual o dialogo de erro deverá aparecer caso
     * o metodo não consiga conectar-se a base de dados.
     * 
     * @throws java.lang.Exception
     */
    public void conectarBancoDados() throws Exception{        
            Class.forName("org.sqlite.JDBC");
            conexaoBanco = DriverManager.getConnection("jdbc:sqlite:"+nomeBanco);
            comandoSql = conexaoBanco.createStatement();      
    }
    
    /**
     * Metodo responsavel por criar a base de dados da aplicação, a base de dados é criada a partir de um comando em SQL
     * armazenado em uma String, este metodo recebe como parametro um objeto do tipo Component que por sua vez
     * é a tela referente ao qual o dialogo do erro deverá aparecer caso ocorra algum erro durante a criação do banco de 
     * dados.
     * 
     * @param telaReferente - Tela onde o dialogo deve estourar se ocorrer um erro
     */
    public void criarBancoDados(Component telaReferente){
        try{
                        
            comandoSql = conexaoBanco.createStatement();
            
            //Criando Tabela de Cache
            comandoSql.execute("CREATE TABLE IF NOT EXISTS [TB_CACHE]("
                    + "[NOME_REDE] [VARCHAR(52)] NOT NULL, "
                    + "[SENHA_REDE] [VARCHAR(52)] NOT NULL"
                    + ");");
            
            //Inserindo valores default na tabela de configurações
            comandoSql.execute("INSERT INTO TB_CACHE VALUES (\"Rede Pessoal\", \"1234567890\");");
            
        }catch(SQLException erroCriacao){
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possivel criar a base de dados, assinatura do erro:\n"+
                    erroCriacao, "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Metodo responsavel por finalizar conexão com o banco de dados, este metodo recebe como parametro um objeto 
     * do tipo Component que por sua vez é a tela referente na qual o dialogo de erro deverá aparecer caso ocorra uma 
     * falha ao tentar finalizar a conexão.
     * 
     * @param telaReferente  - Tela onde o dialogo deve estourar se ocorrer um erro
     */
    public void fecharConexao(Component telaReferente){
        try{
            
            conexaoBanco.close();
            
        }catch(SQLException erroSql){
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possivel finalizar a conexão com o banco, assinatura do erro:\n"+
                    erroSql, "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
