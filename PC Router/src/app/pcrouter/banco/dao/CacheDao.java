package app.pcrouter.banco.dao;

import app.datasound.execute.DataSound;
import app.datasound.execute.DialogSound;
import app.pcrouter.banco.ConexaoSQLite;
import app.pcrouter.banco.tbs.Cache;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * Classe responsavel por efetuar as rotinas básicas de insert, delete, select e update da tabela de cache
 * 
 * @author Tarcísio de Lima
 */
public class CacheDao {
    
    /**
     * Metodo responsável por salvar o nome e a senha da rede toda a vez que o programa for fechado
     * @param telaReferente 
     */
    public void atualizarCache(Component telaReferente){
        ConexaoSQLite conexaoBanco = new ConexaoSQLite();
        
        try{
            
            conexaoBanco.conectarBancoDados();
            ConexaoSQLite.comandoSql = ConexaoSQLite.conexaoBanco.createStatement();
            ConexaoSQLite.comandoSql.execute("UPDATE TB_CACHE SET "
                    + "NOME_REDE=\""+Cache.nomeRede+"\","
                    + "SENHA_REDE=\""+Cache.senhaRede+"\";");
            
        }catch(Exception erroSql){
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possivel armazenar em cache o nome e senha da rede,"
                    + " assinatura do erro:\n"+erroSql.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
            
        }finally{
            
            conexaoBanco.fecharConexao(telaReferente);
        
        }
    }
    
    /**
     * Metodo responsavel por carregar o nome e senha da rede usado da última vez na aplicação.
     * 
     * @param telaReferente  - tela na qual deve aparecer o dialog no caso de erro
     */
    public void carregarCache(Component telaReferente){
        
        ConexaoSQLite conexaoBanco = new ConexaoSQLite();
        
        try{
            
            conexaoBanco.conectarBancoDados();
            ResultSet consultaBanco = ConexaoSQLite.comandoSql.executeQuery("SELECT * FROM TB_CACHE;");
            
            if(consultaBanco != null){
                Cache.nomeRede = consultaBanco.getString("NOME_REDE");
                Cache.senhaRede = consultaBanco.getString("SENHA_REDE");
            }
            
        }catch(Exception erroSql){
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possivel carregar o cache do nome e senha da rede,"
                    + " assinatura do erro:\n"+erroSql.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
            
        }finally{
            conexaoBanco.fecharConexao(telaReferente);
        }
        
    }
    
}
