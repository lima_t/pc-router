package app.pcrouter.telas;

import app.dataimage.execute.*;
import app.datasound.execute.DataSound;
import app.datasound.execute.DialogSound;
import app.pcrouter.banco.tbs.Cache;
import app.pcrouter.classes.Functions;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

/**
 * Classe responsável por montar e exibir a tela principal da aplicação
 * 
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class TelaPrincipal extends javax.swing.JFrame {
    
    private boolean emptyObject = true;
    private static TrayIcon iconeBandeja = null;
    
    //Construtor da tela principal
    public TelaPrincipal() {
        initComponents();//Inicia os componentes da tela
        this.setLocationRelativeTo(null);//Inicia a tela no centro do monitor
        
        //Listener para definir que a janela deverá ser minimizada para a tray ao ser fechada
        addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent e){
                minMaxPrograma(true);
            }
        
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupTray = new java.awt.PopupMenu();
        itemRestaurarJanela = new java.awt.MenuItem();
        itemHomePage = new java.awt.MenuItem();
        itemManualUsuario = new java.awt.MenuItem();
        itemSairPrograma = new java.awt.MenuItem();
        lbNomeRede = new javax.swing.JLabel();
        txtNomeRede = new javax.swing.JTextField();
        checkVerSenha = new javax.swing.JCheckBox();
        txtSenhaRede = new javax.swing.JPasswordField();
        painelStatus = new javax.swing.JPanel();
        lbStatus = new javax.swing.JLabel();
        btOnOff = new javax.swing.JToggleButton();
        barraMenu = new javax.swing.JMenuBar();
        menuPrograma = new javax.swing.JMenu();
        itemTrayIcon = new javax.swing.JMenuItem();
        itemSair = new javax.swing.JMenuItem();
        menuComando = new javax.swing.JMenu();
        itemLimparCache = new javax.swing.JMenuItem();
        itemPlacaRede = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        itemManual = new javax.swing.JMenuItem();
        itemSuporte = new javax.swing.JMenuItem();
        itemSobre = new javax.swing.JMenuItem();

        popupTray.setLabel("PC-Router Menu");

        itemRestaurarJanela.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        itemRestaurarJanela.setLabel("Restauar Janela");
        itemRestaurarJanela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRestaurarJanelaActionPerformed(evt);
            }
        });
        popupTray.add(itemRestaurarJanela);

        itemHomePage.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        itemHomePage.setLabel("Visitar Página do Suporte");
        itemHomePage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemHomePageActionPerformed(evt);
            }
        });
        popupTray.add(itemHomePage);

        itemManualUsuario.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        itemManualUsuario.setLabel("Manual do Usuário");
        itemManualUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemManualUsuarioActionPerformed(evt);
            }
        });
        popupTray.add(itemManualUsuario);
        popupTray.addSeparator();
        itemSairPrograma.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        itemSairPrograma.setLabel("Sair do Programa");
        itemSairPrograma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSairProgramaActionPerformed(evt);
            }
        });
        popupTray.add(itemSairPrograma);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("PC-Router");
        setIconImage(new DataImage().iconeJanela(Image128.ICONE_APLICATIVO)
        );
        setResizable(false);

        lbNomeRede.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lbNomeRede.setText("Nome da Rede:");

        txtNomeRede.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        txtNomeRede.setText(Cache.nomeRede
        );
        txtNomeRede.setToolTipText("<html>\nDigite da rede que o roteador deverá mostrar durante a busca pela Wi-fi<br/>\n(Caracteres estranhos não serão permitidos portanto serão irrelevados).\n</html>");
        txtNomeRede.setNextFocusableComponent(checkVerSenha);
        txtNomeRede.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNomeRedeKeyReleased(evt);
            }
        });

        checkVerSenha.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        checkVerSenha.setText("Senha da Rede (Não Visível):");
        checkVerSenha.setToolTipText("<html>\nAtiva e desativa a visualização da senha da rede durante<br/>\na edição do hotspot.\n</html>");
        checkVerSenha.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        checkVerSenha.setNextFocusableComponent(txtSenhaRede);
        checkVerSenha.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                checkVerSenhaStateChanged(evt);
            }
        });

        txtSenhaRede.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        txtSenhaRede.setText(Cache.senhaRede);
        txtSenhaRede.setToolTipText("Digite a senha da rede Wi-fi (A senha deve ser entre 8 e 32 caracteres).");
        txtSenhaRede.setNextFocusableComponent(btOnOff);
        txtSenhaRede.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSenhaRedeKeyReleased(evt);
            }
        });

        painelStatus.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri", 0, 14), new java.awt.Color(0, 0, 0))); // NOI18N
        painelStatus.setToolTipText("Status do compartilhamento da rede.");
        painelStatus.setNextFocusableComponent(btOnOff);

        lbStatus.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lbStatus.setForeground(new java.awt.Color(255, 0, 0));
        lbStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbStatus.setText("Parado");
        lbStatus.setNextFocusableComponent(btOnOff);

        javax.swing.GroupLayout painelStatusLayout = new javax.swing.GroupLayout(painelStatus);
        painelStatus.setLayout(painelStatusLayout);
        painelStatusLayout.setHorizontalGroup(
            painelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelStatusLayout.createSequentialGroup()
                .addGap(156, 156, 156)
                .addComponent(lbStatus)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelStatusLayout.setVerticalGroup(
            painelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelStatusLayout.createSequentialGroup()
                .addComponent(lbStatus)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        btOnOff.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        btOnOff.setIcon(new DataImage().iconeComponente(Image36.BT_TURN_OFF)
        );
        btOnOff.setText("Ligar Roteador");
        btOnOff.setToolTipText("Liga/Desliga o roteamento da rede.");
        btOnOff.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btOnOff.setNextFocusableComponent(txtNomeRede);
        btOnOff.setPreferredSize(new java.awt.Dimension(120, 25));
        btOnOff.setSelectedIcon(new DataImage().iconeComponente(Image36.BT_TURN_ON));
        btOnOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btOnOffActionPerformed(evt);
            }
        });

        menuPrograma.setText("Programa");
        menuPrograma.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N

        itemTrayIcon.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        itemTrayIcon.setIcon(new DataImage().iconeComponente(Image24.ITEM_BANDEJA)
        );
        itemTrayIcon.setText("Tray Icon");
        itemTrayIcon.setToolTipText("Minimizar aplicativo para a bandeja do sistema.");
        itemTrayIcon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemTrayIconActionPerformed(evt);
            }
        });
        menuPrograma.add(itemTrayIcon);

        itemSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        itemSair.setIcon(new DataImage().iconeComponente(Image24.ITEM_SAIR)
        );
        itemSair.setText("Sair");
        itemSair.setToolTipText("Sair do programa.");
        itemSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSairActionPerformed(evt);
            }
        });
        menuPrograma.add(itemSair);

        barraMenu.add(menuPrograma);

        menuComando.setText("Ferramentas");
        menuComando.setToolTipText("");
        menuComando.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N

        itemLimparCache.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        itemLimparCache.setIcon(new DataImage().iconeComponente(Image24.ITEM_LIMPAR)
        );
        itemLimparCache.setText("Limpar Histórico");
        itemLimparCache.setToolTipText("Limpa o nome e senha da rede usado pela última vez.");
        itemLimparCache.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemLimparCacheActionPerformed(evt);
            }
        });
        menuComando.add(itemLimparCache);

        itemPlacaRede.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        itemPlacaRede.setIcon(new DataImage().iconeComponente(Image24.ITEM_SEARCH)
        );
        itemPlacaRede.setText("Placa de Rede");
        itemPlacaRede.setToolTipText("Mostrar um log detalhado da placa de rede para confirma se é compativel com o compartilhamento de rede.");
        itemPlacaRede.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPlacaRedeActionPerformed(evt);
            }
        });
        menuComando.add(itemPlacaRede);

        barraMenu.add(menuComando);

        menuAjuda.setText("Ajuda");
        menuAjuda.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N

        itemManual.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        itemManual.setIcon(new DataImage().iconeComponente(Image24.ITEM_MANUAL)
        );
        itemManual.setText("Manual");
        itemManual.setToolTipText("Arquivo de ajuda do usuário.");
        itemManual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemManualActionPerformed(evt);
            }
        });
        menuAjuda.add(itemManual);

        itemSuporte.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_HOME, 0));
        itemSuporte.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        itemSuporte.setIcon(new DataImage().iconeComponente(Image24.ITEM_SUPORTE)
        );
        itemSuporte.setText("Home Page");
        itemSuporte.setToolTipText("Visitar a página de suporte do desenvolvedor.");
        itemSuporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSuporteActionPerformed(evt);
            }
        });
        menuAjuda.add(itemSuporte);

        itemSobre.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        itemSobre.setIcon(new DataImage().iconeComponente(Image24.ITEM_SOBRE)
        );
        itemSobre.setText("Sobre");
        itemSobre.setToolTipText("Informações adicionais sobre o programa.");
        itemSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSobreActionPerformed(evt);
            }
        });
        menuAjuda.add(itemSobre);

        barraMenu.add(menuAjuda);

        setJMenuBar(barraMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkVerSenha)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSenhaRede)
                            .addComponent(txtNomeRede)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lbNomeRede)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(painelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(107, Short.MAX_VALUE)
                .addComponent(btOnOff, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(103, 103, 103))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbNomeRede)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeRede, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(checkVerSenha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSenhaRede, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(painelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btOnOff, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void itemTrayIconActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemTrayIconActionPerformed
        minMaxPrograma(true);
    }//GEN-LAST:event_itemTrayIconActionPerformed

    private void itemSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSairActionPerformed
        //Atribuir valores para o cahe futuramente
        Cache.nomeRede = txtNomeRede.getText();
        Cache.senhaRede = txtSenhaRede.getText();
        Functions.sairPrograma(TelaPrincipal.this, btOnOff.isSelected());
    }//GEN-LAST:event_itemSairActionPerformed

    private void itemManualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemManualActionPerformed
        Functions.abrirAjuda(TelaPrincipal.this);
    }//GEN-LAST:event_itemManualActionPerformed

    private void itemSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSobreActionPerformed
        new TelaSobre(TelaPrincipal.this, true).setVisible(true);
    }//GEN-LAST:event_itemSobreActionPerformed

    private void txtNomeRedeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeRedeKeyReleased
        txtNomeRede.setText(Functions.limitarCampoTexto(txtNomeRede.getText(), 50));
    }//GEN-LAST:event_txtNomeRedeKeyReleased

    private void txtSenhaRedeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaRedeKeyReleased
        txtSenhaRede.setText(Functions.limitarCampoTexto(txtSenhaRede.getText(), 32));
    }//GEN-LAST:event_txtSenhaRedeKeyReleased

    private void checkVerSenhaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_checkVerSenhaStateChanged
        if(checkVerSenha.isSelected()){
            txtSenhaRede.setEchoChar((char) 0);
            checkVerSenha.setText("Senha da Rede (Visível):");
        }else{
            txtSenhaRede.setEchoChar('•');
            checkVerSenha.setText("Senha da Rede (Não Visível):");
        }
    }//GEN-LAST:event_checkVerSenhaStateChanged

    private void btOnOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btOnOffActionPerformed
       if(btOnOff.isSelected()){
           Functions.ligarRoteamento(TelaPrincipal.this, txtNomeRede.getText(), txtSenhaRede.getText());
       }else{           
           Functions.desligarRoteamento(TelaPrincipal.this);
       }
    }//GEN-LAST:event_btOnOffActionPerformed

    private void itemRestaurarJanelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRestaurarJanelaActionPerformed
        minMaxPrograma(false);
    }//GEN-LAST:event_itemRestaurarJanelaActionPerformed

    private void itemHomePageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemHomePageActionPerformed
        Functions.irUrl(null);
    }//GEN-LAST:event_itemHomePageActionPerformed

    private void itemManualUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemManualUsuarioActionPerformed
        Functions.abrirAjuda(null);
    }//GEN-LAST:event_itemManualUsuarioActionPerformed

    private void itemSairProgramaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSairProgramaActionPerformed
        //Atribuir valores para o cahe futuramente
        Cache.nomeRede = txtNomeRede.getText();
        Cache.senhaRede = txtSenhaRede.getText();
        Functions.sairPrograma(null, btOnOff.isSelected());
    }//GEN-LAST:event_itemSairProgramaActionPerformed

    private void itemSuporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSuporteActionPerformed
        Functions.irUrl(TelaPrincipal.this);
    }//GEN-LAST:event_itemSuporteActionPerformed

    private void itemPlacaRedeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPlacaRedeActionPerformed
        new TelaPlacaRede(TelaPrincipal.this, true).setVisible(true);
    }//GEN-LAST:event_itemPlacaRedeActionPerformed

    private void itemLimparCacheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemLimparCacheActionPerformed
        txtNomeRede.setText("");
        txtSenhaRede.setText("");
    }//GEN-LAST:event_itemLimparCacheActionPerformed
    
    /**
     * Metodo responsável por minimizar o aplicativo para a bandeja do sistema
     * 
     * @author Tarcísio de Lima
     * @version 1.0.0
     */
    private void minMaxPrograma(boolean minMax){
        
        try{
            
            //Consertar essas duas linhas (bug de passagem de valor duas vezes para variavel final)
            final SystemTray bandejaSistema = SystemTray.getSystemTray();
            
            if(minMax){
                if(SystemTray.isSupported()){
                    
                    if(emptyObject){
                        iconeBandeja = new TrayIcon(new DataImage().iconeJanela(Image128.ICONE_APLICATIVO), "PC-Router", popupTray);
                        //iconeBandeja.setPopupMenu();
                        emptyObject = false;
                    }
                    
                    iconeBandeja.setImageAutoSize(true);
                    bandejaSistema.add(iconeBandeja);//Adiciona o ícone à bandeja
                    iconeBandeja.displayMessage("PC-Router Minimizado", "O programa ainda está executando aqui, clique\n"
                            + "com o botão direito do mouse aqui para acessar\nas opções do programa.", TrayIcon.MessageType.INFO);
                    dispose();//Fecha a Tela Principal
                    
                }else{
                    throw new AWTException("SystemTray.isSupported recebeu o valor false (line: 368) ao minimizar");
                }

            }else{
                
                 if(SystemTray.isSupported()){
                    
                    bandejaSistema.remove(iconeBandeja);//Remove o ícone da bandeja do sistema
                    //iconeBandeja = null;
                    TelaPrincipal.this.setVisible(true);//Restaura a Janela Principal
                    
                }else{
                     throw new AWTException("SystemTray.isSupported recebeu o valor false (line: 380) ao remover.");
                }
            }
        }catch(AWTException erroAwt){
            
            new DataSound().reproduzirSom(TelaPrincipal.this, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(TelaPrincipal.this, "Não foi possível minimizar a aplicação para a bandeja do sistema,"
                        + " Assinatura do erro:\n"+erroAwt.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Metodo responsável por alterar os componentes da tela de maneira correta dependendo do evento dos metodos
     * de ligar e desligar o roteamento.
     * 
     * @param defaultOrChanged
     * @see Functions.ligarRoteamento()
     */
    public static void setarComponentes(byte defaultOrChanged){
         switch(defaultOrChanged){
             case 0:                 
                 btOnOff.setText("Desligar Roteador");
                 lbStatus.setText("Roteando...");
                 lbStatus.setForeground(Color.green);

                 txtNomeRede.setEnabled(false);
                 txtSenhaRede.setEnabled(false);
                 itemLimparCache.setEnabled(false);
                 break;
             case 1:
                 btOnOff.setText("Ligar Roteador");
                 lbStatus.setText("Parado");
                 lbStatus.setForeground(Color.red);

                 txtNomeRede.setEnabled(true);
                 txtSenhaRede.setEnabled(true);
                 itemLimparCache.setEnabled(true);
                 break;
             case 2:
                 btOnOff.setSelected(false);
                  break;
             case 3:
                 btOnOff.setSelected(true);
                 break;
             default:
                 new DataSound().reproduzirSom(null, DialogSound.DIALOG_ERROR);
                 JOptionPane.showMessageDialog(null, "Erro ao setar os componentes da tela principal, Assin"
                         + "atura do erro:\n method setarComponentes() receveid value != 0, 1, 2 or 3", "Erro!", JOptionPane.ERROR_MESSAGE);
                 break;
         }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barraMenu;
    private static javax.swing.JToggleButton btOnOff;
    private static javax.swing.JCheckBox checkVerSenha;
    private java.awt.MenuItem itemHomePage;
    private static javax.swing.JMenuItem itemLimparCache;
    private javax.swing.JMenuItem itemManual;
    private java.awt.MenuItem itemManualUsuario;
    private javax.swing.JMenuItem itemPlacaRede;
    private java.awt.MenuItem itemRestaurarJanela;
    private javax.swing.JMenuItem itemSair;
    private java.awt.MenuItem itemSairPrograma;
    private javax.swing.JMenuItem itemSobre;
    private javax.swing.JMenuItem itemSuporte;
    private javax.swing.JMenuItem itemTrayIcon;
    private javax.swing.JLabel lbNomeRede;
    private static javax.swing.JLabel lbStatus;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenu menuComando;
    private javax.swing.JMenu menuPrograma;
    private javax.swing.JPanel painelStatus;
    private java.awt.PopupMenu popupTray;
    private static javax.swing.JTextField txtNomeRede;
    private static javax.swing.JPasswordField txtSenhaRede;
    // End of variables declaration//GEN-END:variables
}
