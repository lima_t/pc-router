package app.pcrouter.telas;

import app.dataimage.execute.*;
import app.pcrouter.classes.Functions;
import java.awt.Color;

/**
 * Classe responsavel por montar e exibir a tela de informações adicionais do programa.
 * 
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class TelaSobre extends javax.swing.JDialog {

    public TelaSobre(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();//Inicia os componentes
        this.setLocationRelativeTo(parent);//Inicia a tela sobre o centro da Jframe principal.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lb1 = new javax.swing.JLabel();
        painelInfo = new javax.swing.JPanel();
        lbInfo = new javax.swing.JLabel();
        lbLogo = new javax.swing.JLabel();
        lb2 = new javax.swing.JLabel();
        lbHomePage = new javax.swing.JLabel();
        lb3 = new javax.swing.JLabel();
        lbEmail = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Informações Adicionais");
        setIconImage(new DataImage().iconeJanela(Image128.ICONE_SOBRE)
        );
        setResizable(false);

        lb1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lb1.setText("PC-Router");

        painelInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações Adicionais", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri", 0, 12), new java.awt.Color(0, 0, 0))); // NOI18N

        lbInfo.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lbInfo.setText("<html>\n<b>Aplicativo:</b> PC-Router®<br/>\n<b>Versão:</b> 1.1.0<br/>\n<b>Compilação:</b> 0002<br/>\n<b>Ano:</b> 2014<br/>\n<b>Desenvolvedor:</b> Tarcísio de Lima®<br/>\n<b>JDK:</b> Java 7 u45\n</html>");

        lbLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbLogo.setIcon(new DataImage().iconeComponente(Image128.ICONE_APLICATIVO)
        );
        lbLogo.setToolTipText("Pr-Router");
        lbLogo.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lbLogo.setMaximumSize(new java.awt.Dimension(128, 128));
        lbLogo.setMinimumSize(new java.awt.Dimension(128, 128));
        lbLogo.setPreferredSize(new java.awt.Dimension(128, 128));

        javax.swing.GroupLayout painelInfoLayout = new javax.swing.GroupLayout(painelInfo);
        painelInfo.setLayout(painelInfoLayout);
        painelInfoLayout.setHorizontalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelInfoLayout.setVerticalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(painelInfoLayout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(lbInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lb2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lb2.setText("Suporte:");

        lbHomePage.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lbHomePage.setForeground(new java.awt.Color(0, 0, 204));
        lbHomePage.setText("http://www.facebook.com.br/tarcisio.de.lima.amorim");
        lbHomePage.setToolTipText("Visitar página do desenvolvedor no caso de ocorrências inesperadas.");
        lbHomePage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbHomePage.setNextFocusableComponent(lbEmail);
        lbHomePage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbHomePageMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbHomePageMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbHomePageMouseExited(evt);
            }
        });

        lb3.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lb3.setText("E-mail:");

        lbEmail.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lbEmail.setForeground(new java.awt.Color(255, 204, 0));
        lbEmail.setText("tarcisio.lima.amorim@outlook.com");
        lbEmail.setToolTipText("E-mail para contato e suporte.");
        lbEmail.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbEmail.setNextFocusableComponent(lbHomePage);
        lbEmail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbEmailMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbEmailMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbEmailMouseExited(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lb1)
                .addGap(154, 154, 154))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(painelInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lb2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbHomePage))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lb3)
                                .addGap(18, 18, 18)
                                .addComponent(lbEmail)))
                        .addGap(0, 21, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lb1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb2)
                    .addComponent(lbHomePage))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb3)
                    .addComponent(lbEmail))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lbHomePageMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbHomePageMouseEntered
        lbHomePage.setForeground(Color.magenta);
    }//GEN-LAST:event_lbHomePageMouseEntered

    private void lbHomePageMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbHomePageMouseExited
        lbHomePage.setForeground(Color.blue);
    }//GEN-LAST:event_lbHomePageMouseExited

    private void lbEmailMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbEmailMouseEntered
        lbEmail.setForeground(Color.magenta);
    }//GEN-LAST:event_lbEmailMouseEntered

    private void lbEmailMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbEmailMouseExited
        lbEmail.setForeground(new Color(255, 204, 0));
    }//GEN-LAST:event_lbEmailMouseExited

    private void lbHomePageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbHomePageMouseClicked
        lbHomePage.setForeground(Color.red);
        Functions.irUrl(TelaSobre.this);
    }//GEN-LAST:event_lbHomePageMouseClicked

    private void lbEmailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbEmailMouseClicked
        lbEmail.setForeground(Color.red);
        Functions.abrirClienteEmail(TelaSobre.this);
    }//GEN-LAST:event_lbEmailMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb2;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lbEmail;
    private javax.swing.JLabel lbHomePage;
    private javax.swing.JLabel lbInfo;
    private javax.swing.JLabel lbLogo;
    private javax.swing.JPanel painelInfo;
    // End of variables declaration//GEN-END:variables
}
