package app.pcrouter.telas;

import app.pcrouter.classes.Functions;
import app.dataimage.execute.DataImage;
import app.dataimage.execute.Image128;

/**
 *
 * @author Tarcísio de Lima
 */
public class TelaPlacaRede extends javax.swing.JDialog {

    public TelaPlacaRede(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(parent);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barraRolagem = new javax.swing.JScrollPane();
        txtInfoPlacaRede = new javax.swing.JTextArea();
        lbTitulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Informações da Rede Sem Fio");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImage(new DataImage().iconeJanela(Image128.ICONE_SEARCH));
        setMaximumSize(new java.awt.Dimension(550, 450));
        setMinimumSize(new java.awt.Dimension(550, 450));
        setModal(true);
        setResizable(false);

        barraRolagem.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        txtInfoPlacaRede.setEditable(false);
        txtInfoPlacaRede.setColumns(20);
        txtInfoPlacaRede.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtInfoPlacaRede.setLineWrap(true);
        txtInfoPlacaRede.setRows(5);
        txtInfoPlacaRede.setText(Functions.capturarInfoPlacaRede().replaceAll("Æ", "çã").replaceAll("¡", "í")
        );
        txtInfoPlacaRede.setToolTipText("Informações sobre a placa de rede wireless do computador.");
        barraRolagem.setViewportView(txtInfoPlacaRede);

        lbTitulo.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lbTitulo.setText("Placa Wi-Fi");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(barraRolagem)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(231, 231, 231)
                .addComponent(lbTitulo)
                .addContainerGap(237, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(barraRolagem, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane barraRolagem;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JTextArea txtInfoPlacaRede;
    // End of variables declaration//GEN-END:variables
}
