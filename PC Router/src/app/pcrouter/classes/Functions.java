package app.pcrouter.classes;

import app.datasound.execute.DataSound;
import app.datasound.execute.DialogSound;
import app.pcrouter.banco.dao.CacheDao;
import app.pcrouter.telas.TelaPrincipal;
import java.awt.Component;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.JOptionPane;

/**
 * Classe responsável por armazenar metodos importantes da programa.
 * 
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class Functions {
        
    /**
     * Metodo responsável por sair do programa e finalizar o roteamento durante a saída do progama, isto é, se
     * o compartilhamento da rede estiver ativo.
     * 
     * @param telaReferente - Tela na qual o Dialog deve aparecer.
     * @param statusRoteamento - Armazena o Status do roteamento.
     */
    public static void sairPrograma(Component telaReferente, boolean statusRoteamento){
        if(statusRoteamento){
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_INFORMATION);
            
            if(JOptionPane.showConfirmDialog(telaReferente, "Você realmente deseja sair do programa? (Sair do programa"
                    + " fará com que o roteamento pare)", "Aviso!", JOptionPane.YES_NO_OPTION) == 0){
                try{
                
                    Runtime.getRuntime().exec("netsh wlan stop hostednetwork");
                    new CacheDao().atualizarCache(telaReferente);
                    System.exit(0);
                
                }catch(IOException erroSaida){
                  
                    TelaPrincipal.setarComponentes((byte) 3);
                    new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
                    JOptionPane.showMessageDialog(telaReferente, "Não foi possível finalizar o Hotspot devido a um erro,\n"
                            + "tente novamente, Assinatura do erro: " + erroSaida.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);

                }
            }
            
        }else{
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_INFORMATION);
             if(JOptionPane.showConfirmDialog(telaReferente, "Você realmente deseja sair do programa?", "Aviso!",
                     JOptionPane.YES_NO_OPTION) == 0){
                 new CacheDao().atualizarCache(telaReferente);
                System.exit(0);
            }
             
        }
    }
    
    /**
     * Metodo responsável por iniciar o roteamento da internet no computador atráves de um comando que joga
     * argumentos direto para o prompt de comando do Windows.
     * 
     * @param telaReferente - Tela na qual o dialog deve aparecer em caso de erro ou sucesso.
     * @param nomeRede - String com o SSID da rede.
     * @param senhaRede - String com a senha WPA2 da rede.
     */
    public static void ligarRoteamento(Component telaReferente, String nomeRede, String senhaRede){
        
        Process processoCmd = null;
        
        try{
            
            if(nomeRede.length() > 0 && senhaRede.length() >= 8){
                
                //Efetua os comandos de roteamento
                Runtime.getRuntime().exec("netsh wlan set hostednetwork mode=allow ssid=\""+nomeRede+"\" key=\""+senhaRede+"\"");
                processoCmd = Runtime.getRuntime().exec("netsh wlan start hostednetwork");
                InputStream objLeitura = processoCmd.getInputStream();
                
                //Variaveis
                String retornoCmd = "";
                int caractereAscii = 0;
                
                while((caractereAscii = objLeitura.read()) != -1){
                    retornoCmd += (char) caractereAscii;
                }
                
                if(retornoCmd.contains("A rede hospedada foi iniciada")){
                
                    TelaPrincipal.setarComponentes((byte) 0);

                    new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_SUCESS);
                    JOptionPane.showMessageDialog(telaReferente, "O compartilhamento da rede foi criado e iniciado com sucesso,\n"
                            + "verifique na busca por redes do seu dispositivo se ele encontrou\n a rede \"" + nomeRede + "\"", "Sucesso!",
                            JOptionPane.INFORMATION_MESSAGE);

                }else{
                    throw new IOException(retornoCmd);
                }
                
            }else{
                
                TelaPrincipal.setarComponentes((byte) 2);
                
                new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_WARNING);
                JOptionPane.showMessageDialog(telaReferente, "O nome ou senha da rede foram preenchidos incorretamente, verifique os\n"
                        + "valores digitados (Nome da rede não pode ficar vazio nem conter caracteres estranhos\nSenha da rede"
                        + " não pode ficar em branco ou conter menos de 8 caracteres). ", "Aviso!", JOptionPane.WARNING_MESSAGE);
            }
            
        }catch(IOException erroSaida){
        
            TelaPrincipal.setarComponentes((byte) 2);
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possível configurar e criar o Hotspot devido a um erro,\n"
                    + "Assinatura do erro: "+erroSaida.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
            
        }
    }
    
    /**
     * Metodo responsavel por parar o compartilhamento da rede.
     * 
     * @param telaReferente 
     */
    public static void desligarRoteamento(Component telaReferente){
        
        Process processoCmd = null;
        
        try{
            
            processoCmd = Runtime.getRuntime().exec("netsh wlan stop hostednetwork");//Executa comando no prompt para parar a rede hospedada
            InputStream objLeitura = processoCmd.getInputStream();//cria um objeto InputStream para armazenar os caracteres retornados pelo cmd
            
            String retornoCmd = "";//Irá armazenar a String completa com o retorno do CMD
            int caractereAscii = 0;//Armazena o caractere quando passar pelo objLeitura
            
            /*
            While armazena o caractere do retorno do CMD na variavel caractereAscii enquanto houver caracteres
            para ler, depois verifica se já acabou de ler todos e converte o valor ASCII para um caractere legivel
            concatenando com a String
            */
            while((caractereAscii = objLeitura.read()) != -1){
                retornoCmd += (char) caractereAscii;
            }
            
            if(retornoCmd.contains("A rede hospedada parou")){
                
                TelaPrincipal.setarComponentes((byte) 1);
                new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_SUCESS);
                
            }else{
                throw  new IOException(retornoCmd);
            }
        }catch(IOException erroSaida){
            
            TelaPrincipal.setarComponentes((byte) 3);
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possível finalizar o Hotspot devido a um erro,\n"
                    + "Assinatura do erro: "+erroSaida.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
   
    /**
     * Metodo responsavel por impedir que o campo de texto receba valores inválidos como " ou ' e monitora a quantidade
     * de caracteres digitados pelo usuário.
     * 
     * @param limiteCaracteres - número de caracteres permitido.
     * @param texto - texto a ser tratado no metodo.
     * @return texto
     */
   public static String limitarCampoTexto(String texto, int limiteCaracteres){
       
       if(texto.contains("\"") || texto.contains("'") || texto.contains("\\") || texto.contains("|") || 
               texto.contains("/") || texto.length() > limiteCaracteres){
       
           return texto.substring(0, texto.length() - 1);
           
       }else{
           
           return texto;
           
       }
   }
   
   /**
    * Metodo responsável por abrir as páginas de internet pelo navegador padrão
    * 
    * @param telaReferente - Tela na qual o dialog deve aparece em caso de erro.
    */
   public static void irUrl(Component telaReferente){
       try {
            
            Desktop.getDesktop().browse(new URI("http://www.facebook.com.br/tarcisio.de.lima.amorim"));
            
        } catch (IOException | URISyntaxException erroUrl) {
            
            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente,  "Não foi possível abrir http://www.facebook.com.br/tarcisio.de.lima.amorim"
                    + " o caminho ou URL está incorreto\nAssinatura do erro: "+erroUrl.getMessage(),"Erro!", JOptionPane.ERROR_MESSAGE);
            
        }
   }
   
   /**
    * Metodo responsavel por abrir o arquivo de ajuda do usuário.
    * 
    * @param telaReferente - tela na qual o dialog deve aparece em caso de erro.
    */
    public static void abrirAjuda(Component telaReferente) {
        
        try {

            Desktop.getDesktop().open(new File("C:\\Tarcisio Lima®\\PC Router\\documents\\Manual.pdf"));

        } catch (IOException | IllegalArgumentException erroUrl) {

            new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
            JOptionPane.showMessageDialog(telaReferente, "Não foi possível abrir o arquivo de ajuda do usuário"
                    + " o caminho ou URL está incorreto\nAssinatura do erro: " + erroUrl.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);

        }

    }
   
   /**
    * Metodo responsável por abrir o cliente de E-mail padrão do sistema operacional com um cabeçalho pronto para
    * ser enviado para o E-mail do suporte
    *
    * @param telaReferente - Tela na qual o dialog deve aparecer caso ocorra algum erro.
    */
   public static void abrirClienteEmail(Component telaReferente){
       try{
           
           Desktop.getDesktop().mail(new URI("mailto:tarcisio.lima.amorim@outlook.com?subject=PC-Router%20Relatório"
                   + "%20-%20"+System.getProperty("user.name")));
           
       }catch(IOException | URISyntaxException erroEnvio){
           
           new DataSound().reproduzirSom(telaReferente, DialogSound.DIALOG_ERROR);
           JOptionPane.showMessageDialog(telaReferente, "Não foi possível abrir o cliente de E-mail do seu computador,"
                   + "\ncertifique-se de que há um programa instalado, Assinatura do erro:\n"+erroEnvio.getMessage(), "Erro!", 
                    JOptionPane.ERROR_MESSAGE);
       }
   }
   
   /**
    * Metodo reponsavel por ler todas as informações disponiveis da interface wireless do computador e retornar
    * uma String formata para que o chama.
    * 
    * @return retornoCmd
    */
   public static String capturarInfoPlacaRede(){
       
       Process processoCmd = null;
       
       try{
           
           processoCmd = Runtime.getRuntime().exec("netsh wlan show drivers");
           InputStream objLeitura = processoCmd.getInputStream();
           
           //Variaveis para armazenar
           String retornoCmd = "";
           int caractereAscii = 0;
           
           while((caractereAscii = objLeitura.read()) != -1){
               retornoCmd += (char) caractereAscii;
           }
           
           return retornoCmd;
           
       }catch(IOException erroSaida){
           return erroSaida.getMessage();
       }
   }
}
